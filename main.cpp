#include<iostream>
#include<fstream>
#include<string>
#include <chrono>
#include <thread>
#include <iomanip>
#include<Windows.h>
using namespace std;
using namespace this_thread;
using namespace chrono;
void printArtwhite();
void printArtBlue();
string homeworkQ1();
string homeworkQ2();
string homeworkQ3();
string homeworkQ4();
string homeworkQ5();
string homeworkQ6();
int main() {
	SetConsoleTitle("Homework 1.exe");
	int  x;
	string y;
	srand(time(0));
	x = rand() % 2;
	cout << x << endl;
	if (x == 1) {
		printArtBlue();

	}
	else {
		printArtwhite();
	}
	cout << " ______________________________________________________________\n";
	cout << "|v:1.0 | by: khalid al_heji | app:Homework 1 | 2018            |\n";
	cout << "|______________________________________________________________|\n";
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 15);
	string loding[] = { ".!","...!",".....!","......!","......!" };
	ofstream Homework1;
	Homework1.open("HomeWork1.txt");
	ifstream HomeWorkReadOnly;
	if (Homework1.is_open()) {
		string firstName,fimalyName,ID;
		cout << "please enter your first name......?" << endl;
		cin >> firstName;
		cout << "please enter your last name ......?" << endl;
		cin >> fimalyName;
		cout << "plase enter your  academic number..... ?" << endl;
		cin >> ID;

		for (int i = 0; i < sizeof(loding -1); i++)
		{
			
			cout << "please wait" <<loding[i]<< endl;
			sleep_for(milliseconds(1000));
			system("cls");
		}
		cout << "The code is being written >>> :)" << endl;
		sleep_for(milliseconds(2000));
		system("cls");
		Homework1 << "//    " <<" Name :" << firstName <<" "<< fimalyName << "        " << endl;
		Homework1 << "//    " << " ID :"  << ID << "      " << endl;
		Homework1 << "\n";
		Homework1 << "\n";
		cout <<  setw(14) << "Write the question 1 code" << endl;
		cout <<  setw(14) << "Done!" << endl;
		sleep_for(milliseconds(500));
		Homework1 << "\n";
		Homework1 << "\n";
		Homework1 << homeworkQ2();
		cout << setw(14) << "Write the question 2 code" << endl;
		cout << setw(14) << "Done!" << endl;
		sleep_for(milliseconds(500));
		Homework1 << "\n";
		Homework1 << "\n";
		Homework1 << homeworkQ3();
		cout << setw(14) << "Write the question 3 code" << endl;
		cout << setw(14) << "Done!" << endl;
		sleep_for(milliseconds(500));
		Homework1 << "\n";
		Homework1 << "\n";
		Homework1 << homeworkQ4();
		cout << setw(14) << "Write the question 4 code" << endl;
		cout << setw(14) << "Done!" << endl;
		sleep_for(milliseconds(500));
		Homework1 << "\n";
		Homework1 << "\n";
		Homework1 << homeworkQ5();
		cout << setw(14) << "Write the question 5 code" << endl;
		cout << setw(14) << "Done!" << endl;
		sleep_for(milliseconds(500));
		Homework1 << "\n";
		Homework1 << "\n";
		Homework1 << homeworkQ6();
		cout << setw(14) << "Write the question 6 code" << endl;
		cout << setw(14) << "Done!" << endl;
		sleep_for(milliseconds(500));
		string loding[] = { "====","========","","==============","============>" };
		for (int i = 0; i < sizeof(loding-1); i++)
		{
			
			cout << loding[i] << setw(14) <<"writing..!" << endl;
			sleep_for(milliseconds(1000));
			cout << setw(14) << "Write the question 1 code line " << i << endl;
			cout << setw(14) << "Done!" << endl;
			sleep_for(milliseconds(500));
			cout << setw(14) << "Write the question 2 code line " << i << endl;
			cout << setw(14) << "Done!" << endl;
			sleep_for(milliseconds(500));
			cout << setw(14) << "Write the question 3 code line " << i << endl;
			cout << setw(14) << "Done!" << endl;
			sleep_for(milliseconds(500));
			cout << setw(14) << "Write the question 4 code line " << i << endl;
			cout << setw(14) << "Done!" << endl;
			sleep_for(milliseconds(500));
			cout << setw(14) << "Write the question 5 code line " << i << endl;
			cout << setw(14) << "Done!" << endl;
			sleep_for(milliseconds(500));
			cout << setw(14) << "Write the question 6 code line " << i << endl;
			cout << setw(14) << "Done!" << endl;
			system("cls");
			if (i == 4); {
				
				sleep_for(milliseconds(500));
				cout << loding[i] << setw(14) << "ok..!" << endl;
				cout << loding[i] << setw(14) << "Code copied to >>>>>>> /HomeWork1.txt " << endl;
				cout << loding[i] << setw(14) << "Done!" << endl;
			}
			
		}
		Homework1.close();
		int  x;
		string y;
		srand(time(0));
		x = rand() % 2;
		cout << x << endl;
		if (x == 1) {
			printArtBlue();

		}
		else {
			printArtwhite();
		}
	}
	else {
		cout << "Couldn't open " << endl;
	}
		
		
		
	
		system("pause");
		return 0;
	
}
//Q1.formula to compute the energy:
string homeworkQ1() {
	string code =
		"#include<iostream>\n"
		"using namespace std;\n"
		"int main() {\n"
		"	int kilograms, initialTemperature, finalTemperature, energy;\n"
		"	cout << \"Enter the amount of water in kilograms : \" << endl;\n"
		"	cin >> kilograms;\n"
		"	cout<<\"Enter the initial temperature : \"<< endl;\n"
		"	cin >> initialTemperature;\n"
		"	cout<<\"Enter the final temperature : \" << endl;\n"
		"	cin >> finalTemperature;\n"
		"	 energy = kilograms * (finalTemperature - initialTemperature) * 4184;\n"
		"	cout<<\"The energy needed is \"<< energy << endl;\n"
		"	system(\"pause\");\n"
		"	return 0;\n"
		"}\n"
		;
	return code;
}
//Q2. 2X2 system of linear
string homeworkQ2() {
	string code =
		"#include <iostream> \n"
		"using namespace std;\n"
		"int main() {\n"
		"	int a, b, e, c, d, f;\n"
		"	cout << \"pleas enter a : \" << endl; cin >> a;\n"
		"	cout << \"pleas enter b : \" << endl; cin >> b;\n"
		"	cout << \"pleas enter c : \" << endl; cin >> c;\n"
		"	cout << \"pleas enter d : \" << endl; cin >> d;\n"
		"	cout << \"pleas enter e : \" << endl; cin >> e;\n"
		"	cout << \"pleas enter f : \" << endl; cin >> f;\n"
		"	if ((a*d-d*c)==0)\n"
		"	{\n"
		"		cout << \"no solution\";\n"
		"	}\n"
		"	else {\n"
		"		int x = (e*d - b * f) / (a*d - b * c);\n"
		"		int y = (a*f - e * c) / (a*d - b * c);\n"
		"		cout << \"x = \" << x << \"y = \" << y;\n"
		"	}\n"
		"	system(\"pause\");\n"
		"	\n"
		"	return 0;\n"
		"}\n";
	return code;
	}
//Q3. tow root
string homeworkQ3() {
	string code =
        "#include <iostream>\n"
        "#include <cmath>\n"
		"using namespace std;\n"
		"int main() {\n"
		"	double a,b,c;\n"
		"	cout << \"please enter a : \"<<endl;\n"
		"	cin >> a;\n"
		"	cout << \"please enter b : \" << endl;\n"
		"	cin >>  b ;\n"
		"	cout << \"please enter c : \" << endl;\n"
		"	cin >>  c;\n"
		"	double d = pow(b, 2) - 4 * a*c;\n"
		"	if (d > 0) {\n"
		"		double root1 = (-b + pow(d, 0.5)) / (2 * a);\n"
		"		double root2 = (-b - pow(d, 0.5)) / (2 * a);\n"
		"		cout << \"two root  \" <<  root1 <<  \"and \"  << root2 << endl;"
		"	}\n"
		"	else if (d == 0) {\n"
		"		double root1 = (-b + pow(d, 0.5)) / (2 * a);\n"
		"		cout << \"one root\" << root1 << endl;"
		"	}\n"
		"	else{\n"
		"		cout << \"no real root\" << endl;\n"
		"	}"
		"	"
		"	system(\"pause\");\n"
		"\n"
		"	return 0;\n"
		"}\n";
	return code;
}
//Q4.package(in opunds)
string homeworkQ4() {
	string code =
		"#include<iostream>\n"
		"using namespace std;\n"
		"int main() {\n"
		"	float weight, costPerPound;\n"
		"	cout << \"Enter the weight of the package : \" << endl;\n"
		"	cin >> weight;\n"
		"	if (weight > 50)\n"
		"	{\n"
		"		cout << \"The package cannot be shipped.\" << endl;\n"
		"	}"
		"	else {\n"
		"		 if (weight > 0 && weight <= 1)\n"
		"		{\n"
		"			costPerPound = 3.5;\n"
		"		}\n"
		"		else if (weight <= 3)\n"
		"		{\n"
		"			costPerPound = 5.5;\n"
		"		}\n"
		"		else if (weight <= 10)\n"
		"		{\n"
		"			costPerPound = 8.5;\n"
		"		}\n"
		"		else \n"
		"		{\n"
		"			costPerPound = 10.5;\n"
		"		}\n"
		"		cout << \"Shipping cost of package is $\" << costPerPound * weight << endl;\n"
		"		}\n"

		"	system(\"pause\");\n"
		"	return 0;\n"

		"}\n"
		;
	return code;
}//Q5. table kilograms pound
string homeworkQ5() {
	string code =
		"#include <iostream>\n"
		"#include <iomanip>\n"
		"using namespace std;\n"
		"int main() {\n"
		"	float pound;\n"
		"	float kilograms;\n"
		"	int j = 20;\n"
		"	cout << setw(2)<<\"kilograms\" << setw(8) << \"pound\" << setw(8) << \" | \" << \"pound\" << setw(12) << \"kilograms\" << endl;\n"
		"	for (int i = 1; i <= 199; i+=2)\n"
		"	{\n"
		"		\n"
		"		cout <<setw(5)<< i << setw(12) <<i*2.2 << setw(6) <<\" | \" <<    j << setw(14) << j/2.2 << endl;\n"
		"		j = j + 5;\n"
		"	}\n"
		"\n"
		"	system(\"pause\");\n"
		"	return 0;\n"
		"\n"
		"}\n";
	return code;
}
//Q6. series:
string homeworkQ6() {
	string code =
		"#include <iostream>\n"
		"#include<cmath>\n"
		"#include<iomanip>\n"
		"using namespace std;\n"
		"int main(){\n"
		"	double pi;\n"
		"	double sum = 0;\n"
		"	double value = 10000.0;\n"
		"	for (double i = 1; i <= value; i++)\n"
		"	{\n"
		"		pi = 4 * (pow((-1), (i + 1)) / (2*i-1));\n"
		"		sum += pi;\n"
		"	\n"
		"	}\n"
		"	pi = sum;\n"
		"	cout << setprecision(16) << showpoint << fixed; //use scientific notation\n"
		"	cout << setw(6) << \"for i = 10000 | pi = \" << pi << endl;\n"
		"	sum = 0;\n"
		"	value = 20000.0;\n"
		"	for (double i = 1; i <= value; i++)\n"
		"	{\n"
		"		pi = 4 * (pow((-1), (i + 1)) / (2 * i - 1));\n"
		"		sum += pi;\n"
		"\n"
		"	}\n"
		"	pi = sum;\n"

		"\n"
		"	cout << setw(6) << \"for i = 20000 | pi = \" << pi << endl;\n"
		"	sum = 0;\n"
		"	value = 200000.0;\n"
		"	for (double i = 1; i <= value; i++)\n"
		"	{\n"
		"		pi = 4 * (pow((-1), (i + 1)) / (2 * i - 1));\n"
		"		sum += pi;\n"
		"\n"
		"	}\n"
		"	pi = sum;\n"
		"	cout << setw(6)<<\"for i = 200000 | pi = \" << pi << endl;\n"
		"	system(\"pause\");\n"
		"		return 0;\n"
		"}\n";

	return code;
}
